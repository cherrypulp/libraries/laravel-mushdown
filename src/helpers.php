<?php

if (!function_exists('\mushdown')) {
    function mushdown($string, $data = [], $params = ['nl2br' => false])
    {
        return app(\Blok\Mushdown\Mushdown::class)->toHtml($string, $data, $params);
    }
}
