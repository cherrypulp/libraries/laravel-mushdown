<?php

namespace Blok\Mushdown;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        require_once __DIR__ . '/helpers.php';
    }

    public function register()
    {
        $this->app->singleton('mushdown', function () {
            return new Mushdown();
        });
    }
}
