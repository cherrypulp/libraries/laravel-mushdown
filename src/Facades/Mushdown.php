<?php

namespace Blok\Mushdown\Facades;

use Illuminate\Support\Facades\Facade;

class Mushdown extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mushdown';
    }
}
