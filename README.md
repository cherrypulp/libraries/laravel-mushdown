# Mustache + Shortcode + Markdown parser = Mushdown

[![Build Status](https://travis-ci.org/blok/mushdown.svg?branch=master)](https://travis-ci.org/blok/mushdown)
[![Coverage Status](https://coveralls.io/repos/github/blok/mushdown/badge.svg?branch=master)](https://coveralls.io/github/blok/mushdown?branch=master)
[![Packagist](https://img.shields.io/packagist/v/blok/mushdown.svg)](https://packagist.org/packages/blok/mushdown)
[![Packagist](https://poser.pugx.org/blok/mushdown/d/total.svg)](https://packagist.org/packages/blok/mushdown)
[![Packagist](https://img.shields.io/packagist/l/blok/mushdown.svg)](https://packagist.org/packages/blok/mushdown)

Mushdown, modern parser for modern app.

## Installation

Install via composer

```bash
composer require blok/mushdown
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\Mushdown\ServiceProvider" --tag="config"
```

## Usage

How to register a shortcode ?

The shortcode methode works like Wordpress short code, you have 2 params the attributes and the content.

You can register a shortcode like that :

```php

app('mushdown')->register('quote', function ($attr, $content = null) {
    return '<quote '.Html::attributes($attr).'>' . app('mushdown')->compile($content) . '</quote>';
});

echo mushdown(<<<EOT
# Heading

- list

[quote class="mt-2"]Test quote[/quote]

{{user.name}}

EOT, ['user' => ['name' => 'John']]);
```

will output :

```html
<h1 id="heading">Heading</h1>
<ul>
<li>list</li>
</ul>
<p><quote  class="mt-2">Test quote</quote></p>
<p>test</p>
```

## Order of processing

The Mushdown parser are in this order so it won't make normaly any issue or overlapse :

1. Shortcode : In house implementation
2. Mustache : [https://github.com/bobthecow/mustache.php](https://github.com/bobthecow/mustache.php)
3. Markdown : [https://github.com/spatie/laravel-markdown](https://github.com/spatie/laravel-markdown)

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Daniel Sum](https://github.com/blok/shortcode)
- [All contributors](https://github.com/blok/shortcode/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
